#
# example Dockerfile for http://docs.docker.com/examples/postgresql_service/
#

FROM ubuntu:trusty
MAINTAINER pabloandi@gmail.com

RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
	&& localedef -i es_CO -c -f UTF-8 -A /usr/share/locale/locale.alias es_CO.UTF-8
ENV LANG es_CO.utf8

# Add the PostgreSQL PGP key to verify their Debian packages.
# It should be the same key as https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8

# Add PostgreSQL's repository. It contains the most recent stable release
#     of PostgreSQL, ``9.0``.
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Install ``python-software-properties``, ``software-properties-common`` and PostgreSQL 9.0
#  There are some warnings (in red) that show up during the build. You can hide
#  them by prefixing each apt-get statement with DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y python-software-properties software-properties-common postgresql-9.0 postgresql-client-9.0 postgresql-contrib-9.0

# Note: The official Debian and Ubuntu images automatically ``apt-get clean``
# after each ``apt-get``

# Run the rest of the commands as the ``postgres`` user created by the ``postgres-9.0`` package when it was ``apt-get installed``
USER postgres

ADD Base.backup /tmp/Base.backup
ADD roless9.sql /tmp/roles.sql

# Create a PostgreSQL role named ``docker`` with ``docker`` as the password and
# then create a database `docker` owned by the ``docker`` role.
# Note: here we use ``&&\`` to run commands one after the other - the ``\``
#       allows the RUN command to span multiple lines.
RUN /etc/init.d/postgresql start &&\
    createdb -T template0 -E 'UTF-8' siabuc9 &&\
    droplang plpgsql siabuc9 &&\
    psql -d siabuc9 < /tmp/roles.sql &&\
    pg_restore -O -d siabuc9 /tmp/Base.backup

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible. 
RUN echo "host all  all    0.0.0.0/0  md5" >> /etc/postgresql/9.0/main/pg_hba.conf

# And add ``listen_addresses`` to ``/etc/postgresql/9.0/main/postgresql.conf``
RUN echo "listen_addresses='*'" >> /etc/postgresql/9.0/main/postgresql.conf


# Expose the PostgreSQL port
EXPOSE 5432

# Add VOLUMEs to allow backup of config, logs and databases
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

# Set the default command to run when starting the container
CMD ["/usr/lib/postgresql/9.0/bin/postgres", "-D", "/var/lib/postgresql/9.0/main", "--config_file=/etc/postgresql/9.0/main/postgresql.conf"]
